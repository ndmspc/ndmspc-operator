#!/bin/bash

BASE_GIT="git@github.com:"
MY_VER=${MY_VER-"0.0.1"}
SRC=${1-"ndmspc/community-operators"}
DST=${2-"k8s-operatorhub/community-operators"}
WKDIR="/tmp/community-operators"
OPDIR=$PWD

[ -d $WKDIR ] && { echo "Error: WKDIR '$WKDIR' exists !!! Remove it first !!!"; exit 1; }

git clone ${BASE_GIT}${SRC}.git $WKDIR
cd $WKDIR
git config user.name "ndmspc"
git config user.email "ndmspc@gmail.com"
git remote add upstream ${BASE_GIT}$DST
git fetch --all
git remote -v
# git reset --hard upstream main
git checkout -b $MY_VER
git pull --rebase upstream main
mkdir -p operators/ndmspc-operator/$MY_VER
cp -a $OPDIR/bundle/* operators/ndmspc-operator/$MY_VER/
git add .
git commit -m "Release ndmspc-v$MY_VER" -s
git push origin $MY_VER

curl \
  -X POST \
  -H "Accept: application/vnd.github+json" \
  -H "Authorization: Bearer $NDMSPC_GITHUB_TOKEN"\
  -H "X-GitHub-Api-Version: 2022-11-28" \
  https://api.github.com/repos/$DST/pulls \
  -d '{"title":"ndmspc-v'$MY_VER'","body":"Release ndmspc-v'$MY_VER'","head":"ndmspc:'$MY_VER'","base":"main"}'
