#!/bin/bash

NDMSPC_CONTAINER_TOOL=${NDMSPC_CONTAINER_TOOL-"podman"}
NDMSPC_CLUSTER_TYPE=${NDMSPC_CLUSTER_TYPE-"minikube"}
NDMSPC_K8S_VERSION=${NDMSPC_K8S_VERSION-"v1.28.3"}
NDMSPC_LOADBALANCER=${NDMSPC_LOADBALANCER-1}
NDMSPC_INGRESS=${NDMSPC_INGRESS-1}
NDMSPC_KEYCLOAK=${NDMSPC_KEYCLOAK-1}
NDMSPC_K8S_DASHBOARD=${NDMSPC_K8S_DASHBOARD-1}
NDMSPC_OLM_VERSION=${NDMSPC_OLM_VERSION-"v0.27.0"}
NDMSPC_PROD=${NDMSPC_PROD-1}
NDMSPC_DEV=${NDMSPC_DEV-0}
NDMSPC_APPLY_DEFAULT_CONFIG=${NDMSPC_APPLY_DEFAULT_CONFIG-1}
NDMSPC_K8S_DASHBOARD_FILE=${NDMSPC_K8S_DASHBOARD_FILE-"ndmspc-k8s-dashboard.md"}

if [[ $NDMSPC_DEV == 1 ]];then
  NDMSPC_PROD=0
  NDMSPC_APPLY_DEFAULT_CONFIG=0
fi

MINIKUBE_ADDONS=${MINIKUBE_ADDONS-"ingress metallb metrics-server"}

[[ $NDMSPC_K8S_DASHBOARD == 1 ]] && MINIKUBE_ADDONS="$MINIKUBE_ADDONS dashboard"

export KIND_CLUSTER_NAME=${KIND_CLUSTER_NAME-"kind"}
NDMSPC_RESET=${NDMSPC_RESET-1}

KIND_METALLB_VERSION=${KIND_METALLB_VERSION-"v0.13.7"}
KIND_INGRESS_VERSION=${KIND_INGRESS_VERSION-"v1.9.5"}
KIND_DASHBOARD_VERSION=${KIND_DASHBOARD_VERSION-"v2.7.0"}


if [[ $NDMSPC_CLUSTER_TYPE == "minikube" ]];then

  if ! command -v minikube &> /dev/null;then
      echo "Executable 'minikube' could not be found!!! Please install it first !!!"
      exit 1
  fi


  # [[ $NDMSPC_RESET == 1 ]] && minikube delete --all --purge
  [[ $NDMSPC_RESET == 1 ]] && minikube delete
  minikube start --driver=$NDMSPC_CONTAINER_TOOL --kubernetes-version=$NDMSPC_K8S_VERSION

  for a in $MINIKUBE_ADDONS;do
    minikube addons enable $a
  done  

elif [[ $NDMSPC_CLUSTER_TYPE == "kind" ]];then

  if ! command -v kind &> /dev/null;then
      echo "Executable 'kind' could not be found!!! Please install it first !!!"
      exit 1
  fi

  [[ $NDMSPC_RESET == 1 ]] && kind delete cluster

  echo "Installing kind cluster '$KIND_CLUSTER_NAME' ... "

  if [[ $(id -u) == 0 ]];then
cat <<EOF | kind create cluster --image=kindest/node:$NDMSPC_K8S_VERSION --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  ipFamily: ipv4
nodes:
  - role: control-plane
    kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
      - containerPort: 80
        hostPort: 80
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        protocol: TCP

EOF
  else 
cat <<EOF | kind create cluster --image=kindest/node:$NDMSPC_K8S_VERSION --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  ipFamily: ipv4
nodes:
  - role: control-plane
    kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
      - containerPort: 80
        hostPort: 8008
        protocol: TCP
      - containerPort: 443
        hostPort: 8443
        protocol: TCP

EOF
  fi

  if [[ $NDMSPC_LOADBALANCER == 1 ]];then
    echo "Installing loadbalancer via 'metallb-$KIND_METALLB_VERSION' ..."
    kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/$KIND_METALLB_VERSION/config/manifests/metallb-native.yaml
    while ! kubectl wait --namespace metallb-system --for=condition=ready pod --selector=app=metallb --timeout=90s > /dev/null 2>&1; do
      echo "Waiting for metallb pods will start ..."
      sleep 5
    done

    if [[ $NDMSPC_CONTAINER_TOOL == "podman" ]];then
      METALLB_IP=$(podman network inspect -f '{{range .Subnets}}{{if eq (len .Subnet.IP) 4}}{{.Subnet}}{{end}}{{end}}' $KIND_CLUSTER_NAME)
    else
      METALLB_IP=$(docker network inspect -f '{{.IPAM.Config}}' $KIND_CLUSTER_NAME)
    fi

    METALLB_IP_BASE=$(echo $METALLB_IP | cut -d '.' -f1-2)
    echo $METALLB_IP_BASE

    if [ -n "$METALLB_IP" ];then
cat <<EOF | kubectl apply -f -
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: example
  namespace: metallb-system
spec:
  addresses:
  - ${METALLB_IP_BASE}.255.200-${METALLB_IP_BASE}.255.250
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: empty
  namespace: metallb-system
EOF
    fi
  fi

  if [[ $NDMSPC_INGRESS == 1 ]];then
    echo "Installing 'ingress-nginx-$KIND_INGRESS_VERSION' ..."
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-$KIND_INGRESS_VERSION/deploy/static/provider/kind/deploy.yaml
  fi

  if [[ $NDMSPC_K8S_DASHBOARD == 1 ]];then
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/$KIND_DASHBOARD_VERSION/aio/deploy/recommended.yaml



  fi

fi

echo "Installing 'olm-$NDMSPC_OLM_VERSION' ..."
curl -sL https://github.com/operator-framework/operator-lifecycle-manager/releases/download/$NDMSPC_OLM_VERSION/install.sh | bash -s $NDMSPC_OLM_VERSION

if [[ $NDMSPC_PROD == 1 ]];then
  echo "Subscribing to ndmspc-operator form operatorhub.io ..."
cat <<EOF | kubectl apply -f -
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: my-ndmspc-operator
  namespace: operators
spec:
  channel: alpha
  name: ndmspc-operator
  source: operatorhubio-catalog
  sourceNamespace: olm
EOF
fi

if [[ $NDMSPC_CLUSTER_TYPE == "minikube" ]];then
  if [[ $NDMSPC_KEYCLOAK == 1 ]];then
    kubectl create -f https://raw.githubusercontent.com/keycloak/keycloak-quickstarts/latest/kubernetes/keycloak.yaml

    while ! kubectl wait --namespace default --for=condition=ready pod --selector=app=keycloak --timeout=90s > /dev/null 2>&1; do
      echo "Waiting for keycloak pods will start ..."
      sleep 5
    done

    curl -s https://raw.githubusercontent.com/keycloak/keycloak-quickstarts/latest/kubernetes/keycloak-ingress.yaml | \
    sed "s/KEYCLOAK_HOST/keycloak.$(minikube ip).nip.io/" | \
    kubectl create -f -


    KEYCLOAK_URL=https://keycloak.$(minikube ip).nip.io 
    echo ""
    echo "Keycloak:                 $KEYCLOAK_URL"
    echo "Keycloak Admin Console:   $KEYCLOAK_URL/admin"
    echo "Keycloak Account Console: $KEYCLOAK_URL/realms/myrealm/account"
    echo ""
  fi

  if [[ $NDMSPC_K8S_DASHBOARD == 1 ]];then
    echo ""
    echo "k8s dashboard:" > $NDMSPC_K8S_DASHBOARD_FILE
    echo "  run : \$ minikube dashboard" >> $NDMSPC_K8S_DASHBOARD_FILE
    echo "" >> $NDMSPC_K8S_DASHBOARD_FILE
    cat $NDMSPC_K8S_DASHBOARD_FILE
  fi

elif [[ $NDMSPC_CLUSTER_TYPE == "kind" ]];then
  if [[ $NDMSPC_K8S_DASHBOARD == 1 ]];then

cat <<EOF | kubectl apply -f -
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
EOF

    NDMSPC_K8S_DASHBOARD_TOKEN=$(kubectl -n kubernetes-dashboard create token admin-user)
    echo ""
    echo "k8s dashboard:" > $NDMSPC_K8S_DASHBOARD_FILE
    echo "  run : \$ kubectl proxy" >> $NDMSPC_K8S_DASHBOARD_FILE
    echo "  access url: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/" >> $NDMSPC_K8S_DASHBOARD_FILE
    echo "  token: $NDMSPC_K8S_DASHBOARD_TOKEN" >> $NDMSPC_K8S_DASHBOARD_FILE
    echo ""
    cat $NDMSPC_K8S_DASHBOARD_FILE
    echo ""
    echo "Above info is stored in '$NDMSPC_K8S_DASHBOARD_FILE' for later use"
    echo ""

  fi
fi



if [[ $NDMSPC_APPLY_DEFAULT_CONFIG == 1 ]];then
  echo ""
  echo "To apply default config do :"

  if [[ $NDMSPC_CLUSTER_TYPE == "minikube" ]];then
  echo "  curl -s https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/config/samples/apps_v1alpha1_ndmspcconfig.yaml | sed 's/127.0.0.1.nip.io/$(minikube ip).nip.io/' | kubectl create -f -"
  else
  echo "  kubectl apply -f https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/config/samples/apps_v1alpha1_ndmspcconfig.yaml"
  fi
  echo ""
fi