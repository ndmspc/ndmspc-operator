# ndmspc-operator

## Installation

### Quick install

```bash
bash <(curl -s https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/scripts/install.sh)
```

This script will do the steps described below.

### KIND k8s cluster

For docker, setup make sure that docker is running

For kind make sure that one has limits set to higher values.
By default, the limits are set to 1024.
One can set the limits to higher values by setting it in following file

```bash
cat ~/.config/containers/containers.conf
[containers]
pids_limit = 0
default_ulimits = [
  "nofile=8192:524288",
]
```

Install local k8s cluster with [kind](https://kind.sigs.k8s.io/).
For our setup, one has to use an additional `kind` of configuration.

```bash

cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches: - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 8008
    protocol: TCP
  EOF

```

One should see the following output

```bash
enabling experimental podman provider
Creating cluster "kind" ...
✓ Ensuring node image (kindest/node:v1.27.3) 🖼
✓ Preparing nodes 📦
 ✓ Writing configuration 📜
✓ Starting control-plane 🕹️
✓ Installing CNI 🔌
✓ Installing StorageClass 💾
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Not sure what to do next? 😅 Check out <https://kind.sigs.k8s.io/docs/user/quick-start/>

```

### Install ingress

To install `ingress` one has to apply the following manifest

```

kubectl apply -f <https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.9.3/deploy/static/provider/kind/deploy.yaml>

```

### Install Operator Lifecycle Manager (OLM)

By running the following script one can install OLM `v0.25.0`

```bash

curl -sL <https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.25.0/install.sh> | bash -s v0.25.0

```

One should see the following output

```bash
customresourcedefinition.apiextensions.k8s.io/catalogsources.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/clusterserviceversions.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/installplans.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/olmconfigs.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/operatorconditions.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/operatorgroups.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/operators.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/subscriptions.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/catalogsources.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/clusterserviceversions.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/installplans.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/olmconfigs.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/operatorconditions.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/operatorgroups.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/operators.operators.coreos.com condition met
customresourcedefinition.apiextensions.k8s.io/subscriptions.operators.coreos.com condition met
namespace/olm created
namespace/operators created
serviceaccount/olm-operator-serviceaccount created
clusterrole.rbac.authorization.k8s.io/system:controller:operator-lifecycle-manager created
clusterrolebinding.rbac.authorization.k8s.io/olm-operator-binding-olm created
olmconfig.operators.coreos.com/cluster created
deployment.apps/olm-operator created
deployment.apps/catalog-operator created
clusterrole.rbac.authorization.k8s.io/aggregate-olm-edit created
clusterrole.rbac.authorization.k8s.io/aggregate-olm-view created
operatorgroup.operators.coreos.com/global-operators created
operatorgroup.operators.coreos.com/olm-operators created
clusterserviceversion.operators.coreos.com/packageserver created
catalogsource.operators.coreos.com/operatorhubio-catalog created
Waiting for deployment "olm-operator" rollout to finish: 0 of 1 updated replicas are available...
deployment "olm-operator" successfully rolled out
Waiting for deployment "catalog-operator" rollout to finish: 0 of 1 updated replicas are available...
deployment "catalog-operator" successfully rolled out
Package server phase: Installing
Package server phase: Succeeded
deployment "packageserver" successfully rolled out

```

### Install ndmspc-operator

<div class="artifacthub-widget" data-url="https://artifacthub.io/packages/olm/community-operators/ndmspc-operator" data-theme="light" data-header="true" data-stars="true" data-responsive="false"><blockquote><p lang="en" dir="ltr"><b>NdmSpc operator</b>: The NdmSpc operator automates the deployment and management of Ndmspc suite on Kubernetes cluster.</p>&mdash; Open in <a href="https://artifacthub.io/packages/olm/community-operators/ndmspc-operator">Artifact Hub</a></blockquote></div><script async src="https://artifacthub.io/artifacthub-widget.js"></script>

One can execute the command shown on page <https://artifacthub.io/packages/olm/community-operators/ndmspc-operator?modal=install>
and run the following snippet

```bash

cat <<EOF | kubectl apply -f -
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: my-ndmspc-operator
  namespace: operators
spec:
  channel: alpha
  name: ndmspc-operator
  source: operatorhubio-catalog
  sourceNamespace: olm
  installPlanApproval: Automatic
EOF

```

Then watch for the operator to be installed by looking into `PHASE` column for Succeeded` value

```bash

[mvala@kbf019-44 config]$ kubectl get csv -n operators
NAME DISPLAY VERSION REPLACES PHASE
ndmspc-operator.v0.2.1 NdmSpc operator 0.2.1 ndmspc-operator.v0.2.0 Succeeded

```

### Start ndmspc cluster

One can apply `NdmSpcConfig` config shown in <https://artifacthub.io/packages/olm/community-operators/ndmspc-operator?modal=crds&file=ndmspcconfigs.apps.ndmspc.io> with all customizations or with defaults shown bellow

```bash
cat <<EOF | kubectl apply -f -
apiVersion: apps.ndmspc.io/v1alpha1
kind: NdmSpcConfig
metadata:
  name: ndmspc-sample
EOF
```

Wait until everything is running

```bash
[mvala@kbf019-44 ndmspc-operator]$ kubectl get all,ingress
NAME READY STATUS RESTARTS AGE
pod/ndmspc-sample-executor-758f989776-7kbb5 1/1 Running 0 6m49s
pod/ndmspc-sample-ndmspc-web-7f5d84d677-dfzx6 1/1 Running 0 7m7s
pod/ndmspc-sample-slsd-6dd75b785d-qzrzz 1/1 Running 0 7m1s
pod/ndmspc-sample-slsr-68c5cf6b47-p7s48 1/1 Running 0 7m2s
pod/ndmspc-sample-slsw-7b5d74d45-qlq5n 1/1 Running 0 7m4s
pod/ndmspc-sample-slsw-7b5d74d45-twpgd 1/1 Running 0 7m4s
pod/ndmspc-sample-zmq2ws-6679b889fd-t9zx2 1/1 Running 0 6m55s

NAME TYPE CLUSTER-IP EXTERNAL-IP PORT(S) AGE
service/kubernetes ClusterIP 10.96.0.1 <none> 443/TCP 19m
service/ndmspc-sample-executor-service ClusterIP 10.96.53.115 <none> 41001/TCP 6m48s
service/ndmspc-sample-ndmspc-web-service ClusterIP 10.96.103.13 <none> 10000/TCP 7m5s
service/ndmspc-sample-sls-discovery-service ClusterIP None <none> 40000/TCP 7m
service/ndmspc-sample-sls-mon-service ClusterIP 10.96.227.204 <none> 5001/TCP 6m58s
service/ndmspc-sample-sls-submitter-service ClusterIP 10.96.197.7 <none> 41000/TCP 6m57s
service/ndmspc-sample-zmq2ws-mgr-service ClusterIP 10.96.120.49 <none> 10000/TCP 6m52s
service/ndmspc-sample-zmq2ws-ws-service ClusterIP 10.96.53.156 <none> 8442/TCP 6m54s

NAME READY UP-TO-DATE AVAILABLE AGE
deployment.apps/ndmspc-sample-executor 1/1 1 1 6m49s
deployment.apps/ndmspc-sample-ndmspc-web 1/1 1 1 7m7s
deployment.apps/ndmspc-sample-slsd 1/1 1 1 7m1s
deployment.apps/ndmspc-sample-slsr 1/1 1 1 7m2s
deployment.apps/ndmspc-sample-slsw 2/2 2 2 7m4s
deployment.apps/ndmspc-sample-zmq2ws 1/1 1 1 6m55s

NAME DESIRED CURRENT READY AGE
replicaset.apps/ndmspc-sample-executor-758f989776 1 1 1 6m49s
replicaset.apps/ndmspc-sample-ndmspc-web-7f5d84d677 1 1 1 7m7s
replicaset.apps/ndmspc-sample-slsd-6dd75b785d 1 1 1 7m1s
replicaset.apps/ndmspc-sample-slsr-68c5cf6b47 1 1 1 7m2s
replicaset.apps/ndmspc-sample-slsw-7b5d74d45 2 2 2 7m4s
replicaset.apps/ndmspc-sample-zmq2ws-6679b889fd 1 1 1 6m55s

NAME CLASS HOSTS ADDRESS PORTS AGE
ingress.networking.k8s.io/ndmspc-sample-ingress-ndmspc nginx zmq2ws.127.0.0.1.nip.io,executor.127.0.0.1.nip.io,ndmspc-web.127.0.0.1.nip.io localhost 80 6m47s

```

When everything is running one can access the service via <http://ndmspc.127.0.0.1.nip.io:8008>

## Develop

```bash
pip3 install ansible ansible-runner kubernetes --user
```

```bash
make install run
```

## Make release

- Make issue and MR
- Find and replace version via vscode
  (example: find all `0.X.0` and replace it with new `0.Y.0`)
- Run

```bash
make bundle
```

- Handle `Ndmspc Config` description and `displayName` to be present in the bundle.
  When doing diff, you can take it from the previous version of csv.

```bash
      description: NdmSpc configuration
      displayName: NdmSpc Config
```

- Push

```bash

podman login -u mvala registry.gitlab.com
make docker-build bundle-build docker-push bundle-push

```

- Merge MR (Push to `main`)
- Make tag from `main` in <https://gitlab.com/ndmspc/ndmspc-operator/-/tags>
- When the release pipeline is green watch
  - <https://github.com/k8s-operatorhub/community-operators/pulls>
  - <https://github.com/redhat-openshift-ecosystem/community-operators-prod/pulls>
